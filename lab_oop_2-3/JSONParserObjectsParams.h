#ifndef JSONPARSEROBJECTSPARAMS_H
#define JSONPARSEROBJECTSPARAMS_H


class JSONParserObjectsParams
{
    public:
        bool isStr;
        bool isCompleted;
        bool isObject;
        bool haveValue;
        JSONParserObjectsParams();
};

#endif // JSONPARSEROBJECTSPARAMS_H
