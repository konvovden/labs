#ifndef FACADE_H
#define FACADE_H

#include "BaseParser.h"
#include "ParseError.h"


class Facade
{
    private:
        BaseParser *_checker;
        std::string _filePath;
        std::vector<std::string> _data;
        ParseError error;
        static std::string toString(std::vector<std::string> &data);
        static std::vector<std::string> toStringVector(std::string &data);

    public:
        Facade(BaseParser *checker);
        void loadData(std::vector<std::string> data);
        void loadData(std::string data);
        void loadDataFromFile();
        void loadDataFromFile(std::string &filePath);
        bool isError();
        std::string getErrorMessage();
        int getErrorPos();
        std::vector<std::string> getData();
        std::string getDataAsString();
        void checkData();
        std::string getFilePath();
        void setFilePath(std::string filePath);
        ~Facade();
};

#endif // FACADE_H
