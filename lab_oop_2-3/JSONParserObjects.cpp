#include "JSONParserObjects.h"

#include "CheckManager.h"
#include "JSONParserObjectsParams.h"

using namespace std;

void JSONParserObjects::check()
{
    _error.clear();
    _text = "\"json\":" + _text;
    checkObject(_text);
}

void JSONParserObjects::checkObject(string &text)
{
    JSONParserObjectsParams params;
    params.isObject = true;
    params.isStr = false;
    params.isCompleted = false;

    for(unsigned int i = 0; i < text.length(); i++)
    {
        if(CheckManager::isGap(text[i]))
            continue;
        if(!_error.isEmpty())
            break;

        if(!params.isStr)
        {
            if(params.isObject)
                checkObjectName(text, params, i);
            else
                checkObjectContent(text, params, i);
        }
        else if(CheckManager::isQuote(text[i]))
        {
            params.isStr = false;
            if(params.isCompleted)
                _error.set("Unexpected " + string(1, text[i]), i);
            params.isCompleted = true;
        }
    }
    if(_error.isEmpty() && (params.isObject || !params.isCompleted))
        _error.set("Expected end of structure", text.length());
}

void JSONParserObjects::checkObjectName(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(params.isCompleted)
        checkObjectNameSeparator(text, params, i);
    else
        checkObjectNameValue(text, params, i);
}

void JSONParserObjects::checkObjectNameSeparator(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(CheckManager::isColon(text[i]))
    {
        params.isObject = false;
        params.isCompleted = false;
    }
    else
        _error.set("Expected " + string(1, CheckManager::getColon()), i);
}

void JSONParserObjects::checkObjectNameValue(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(CheckManager::isQuote(text[i]))
        params.isStr = true;
    else
        _error.set("Expected \", but found " + string(1, text[i]), i);
}

void JSONParserObjects::checkObjectContent(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(params.isCompleted)
        checkObjectContentSeparator(text, params, i);
    else
        checkObjectContentValue(text, params, i);
}

void JSONParserObjects::checkObjectContentSeparator(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    switch(text[i])
    {
        case ',':
        case '}':
            params.isCompleted = false;
            params.isObject = true;
        break;

        default:
            _error.set("Expected value, but " + string(1, text[i]) + " found", i);
    }
}

void JSONParserObjects::checkObjectContentValue(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(CheckManager::isQuote(text[i]))
    {
        params.isStr = true;
        params.isCompleted = false;
    }
    else
    {
        switch(text[i])
        {
            case '}':
            case ',':
                if(CheckManager::isNumber(text[i - 1]))
                {
                    params.isObject = true;
                    params.isCompleted = false;
                }
                else
                    _error.set("Unexpected " + string(1, text[i]), i);
            break;
            case '{':
                isolateObject(text, i);
                params.isCompleted = true;
            break;
            case '[':
                isolateArray(text, i);
                params.isCompleted = true;
            break;
            default:
                if(!CheckManager::isNumber(text[i]))
                    _error.set("Expected value, but found " + string(1, text[i]), i);
                else if(CheckManager::isNumber(text[i + 1]))
                    params.isCompleted = true;
            break;
        }
    }
}

void JSONParserObjects::isolateObject(string &text, unsigned int &i)
{
    int brackets = 1;
    string object = "";

    while(brackets && i < text.length())
    {
        i++;
        if(text[i] == '{' || text[i] == '[')
            brackets++;
        else if(text[i] == '}' || text[i] == ']')
            brackets--;
        if(brackets)
            object += text[i];
    }
    checkObject(object);
}

void JSONParserObjects::isolateArray(string &text, unsigned int &i)
{
    int brackets = 1;
    string object = "";

    while(brackets)
    {
        i++;
        if(CheckManager::isOpeningBracket(text[i]))
            brackets++;
        else if(CheckManager::isClosingBracket(text[i]))
            brackets--;
        if(brackets)
            object += text[i];
    }

    object += " ";

    checkArray(object);
}

void JSONParserObjects::checkArray(string &text)
{
    JSONParserObjectsParams params;
    params.haveValue = false;
    params.isStr = false;

    for(unsigned int i = 0; i < text.length(); i ++)
    {
        if(!_error.isEmpty())
            break;
        if(CheckManager::isGap(text[i]))
            continue;

        if(params.isStr)
        {
            if(!params.haveValue)
                checkArrayContent(text, params, i);
            else
                checkArraySeparator(text, params, i);
        }
    }
}

void JSONParserObjects::checkArrayContent(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    switch(text[i])
    {
        case '"':
            params.isStr = true;
        break;
        case '{':
            isolateObject(text, i);
        break;
        case '[':
            isolateArray(text, i);
        break;
        default:
            if(!CheckManager::isNumber(text[i]))
                _error.set("Expected value, but found " + string(1, text[i]), i);
            else if(!CheckManager::isNumber(text[i + 1]))
                params.haveValue = true;
        break;
    }
}

void JSONParserObjects::checkArraySeparator(string &text, JSONParserObjectsParams &params, unsigned int &i)
{
    if(CheckManager::isComma(text[i]))
        params.haveValue = false;
    else
        _error.set("Expected " + string(1, CheckManager::getComma()) + " but found " + string(1, text[i]), i);
}
