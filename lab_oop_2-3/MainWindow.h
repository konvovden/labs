#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Facade.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ButtonStartCheck_clicked();

    void on_ButtonChooseFile_clicked();

    void on_ButtonLoadData_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
    Facade * _facade;

    void clearHighlight();
    void setHighlight(int pos1, int pos2);
};

int CheckError(int status);
#endif // MAINWINDOW_H
