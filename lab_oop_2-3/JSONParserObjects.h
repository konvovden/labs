#ifndef JSONPARSEROBJECTS_H
#define JSONPARSEROBJECTS_H

#include "JSONParser.h"
#include "JSONParserObjectsParams.h"

class JSONParserObjects : public JSONParser
{
    private:
        int _i;
        bool _isObject;
        bool _isString;
        bool _isCompleted;
        void checkObject(std::string &text);
        void checkObjectName(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkObjectNameSeparator(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkObjectNameValue(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkObjectContent(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkObjectContentSeparator(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkObjectContentValue(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void isolateObject(std::string &text, unsigned int &i);
        void isolateArray(std::string &text, unsigned int &i);
        void checkArray(std::string &text);
        void checkArrayContent(std::string &text, JSONParserObjectsParams &params, unsigned int &i);
        void checkArraySeparator(std::string &text, JSONParserObjectsParams &params, unsigned int &i);


    public:
        void check() override;
};

#endif // JSONPARSEROBJECTS_H
