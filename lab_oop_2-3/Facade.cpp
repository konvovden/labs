#include "Facade.h"

#include <fstream>

using namespace std;

Facade::Facade(BaseParser *checker): _checker(checker)
{

}

Facade::~Facade()
{
    delete _checker;
}

string Facade::getErrorMessage()
{
    return error.getText();
}

int Facade::getErrorPos()
{
    return error.getCharNum();
}

vector<string> Facade::getData()
{
    return _data;
}

void Facade::checkData()
{
    _checker->check(_data);
    error = _checker->getError();
}

string Facade::toString(vector<string> &data)
{
    string result = "";
    size_t size = data.size();
    for(size_t i = 0; i < size; i++)
        result += data[i] + string(1, '\n');

    return result;
}

vector<string> Facade::toStringVector(string &data)
{
    vector<string> result;
    string temp = "";
    for(size_t i = 0; i < data.length(); i++)
    {
        temp += data[i];
        if(data[i] == '\n')
        {
            result.push_back(temp);
            temp = "";
        }
    }
    result.push_back(temp);
    return result;
}

void Facade::loadData(vector<string> data)
{
    _data = data;
}

void Facade::loadData(string data)
{
    loadData(toStringVector(data));
}

bool Facade::isError()
{
    return (!error.isEmpty());
}

string Facade::getFilePath()
{
    return _filePath;
}

void Facade::setFilePath(string filePath)
{
    _filePath = filePath;
}

void Facade::loadDataFromFile()
{
    loadDataFromFile(_filePath);
}

void Facade::loadDataFromFile(string &filePath)
{
    fstream file(filePath);

    _data.clear();
    string line = "";
    while(getline(file, line))
        _data.push_back(line);
}

string Facade::getDataAsString()
{
    return Facade::toString(_data);
}
