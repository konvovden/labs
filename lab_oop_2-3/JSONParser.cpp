#include "JSONParser.h"

#include "CheckManager.h"
#include "JSONParserBrackets.h"
#include "JSONParserObjects.h"

using namespace std;

void JSONParser::check()
{
    _error.clear();
    JSONParserBrackets bracketsParser;
    bracketsParser.setText(_text);
    bracketsParser.check();
    _error = bracketsParser._error;

    if(_error.isEmpty())
    {
        JSONParserObjects objectsParser;
        objectsParser.setText(_text);
        objectsParser.check();
        _error = objectsParser._error;
    }
}
