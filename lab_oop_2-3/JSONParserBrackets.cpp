#include "JSONParserBrackets.h"

#include "CheckManager.h"
#include <stack>

using namespace std;

void JSONParserBrackets::check()
{
    stack<char> st;

    _error.clear();

    bool isStr = false;
    for(size_t i = 0; i < _text.size(); i++)
    {
        if(!isStr)
        {
            if(CheckManager::isOpeningBracket(_text[i]))
                st.push(_text[i]);

            else if (CheckManager::isClosingBracket(_text[i]))
            {
                if(_text[i] != CheckManager::getClosingBracket(st.top()))
                {
                    _error.set("Expected " + string(1, CheckManager::getClosingBracket(st.top())) +
                            ", but found " + string(1, _text[i]), i);
                    break;
                }
                else
                    st.pop();
            }
        }

        if(CheckManager::isQuote(_text[i]))
        {
            isStr = !isStr;
            if(CheckManager::isQuote(st.top()))
                    st.pop();
        }
    }

    if(st.size() && _error.isEmpty())
        _error.set("Expected " + string(1, CheckManager::getClosingBracket(st.top())), _text.size());
}
