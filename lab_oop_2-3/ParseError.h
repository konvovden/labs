#ifndef ERROR_H
#define ERROR_H

#include <string>

class ParseError
{
    private:
        bool _isEmpty;
        std::string _text;
        int _charNum;

    public:
        ParseError();
        ParseError(std::string text, int charNum);
        bool isEmpty();
        std::string getText();
        int getCharNum();
        void clear();
        void set(std::string text, int charNum);

};

#endif // ERROR_H
