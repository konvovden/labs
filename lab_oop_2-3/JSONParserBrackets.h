#ifndef JSONPARSERBRACKETS_H
#define JSONPARSERBRACKETS_H

#include "JSONParser.h"

class JSONParserBrackets: public JSONParser
{
    public:
        void check() override;
};

#endif // JSONPARSERBRACKETS_H
