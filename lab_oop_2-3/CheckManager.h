#ifndef CHECKMANAGER_H
#define CHECKMANAGER_H

class CheckManager
{
    public:
        static bool isGap(char c);
        static bool isQuote(char c);
        static bool isOpeningBracket(char c);
        static bool isClosingBracket(char c);
        static char getClosingBracket(char c);
        static bool isColon(char c);
        static char getColon();
        static char getComma();
        static bool isComma(char c);
        static bool isNumber(char c);
};

#endif // CHECKMANAGER_H
