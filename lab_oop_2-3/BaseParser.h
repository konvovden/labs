#ifndef BASECHECKER_H
#define BASECHECKER_H

#include <string>
#include <vector>

#include "ParseError.h"

class BaseParser
{
    protected:
        std::string _text;
        ParseError _error;

    public:
        BaseParser();
        virtual ~BaseParser();
        virtual void check() = 0;
        virtual void check(std::vector<std::string> &text);
        virtual void check(std::string &text);
        virtual void setText(std::vector<std::string> &text);
        virtual void setText(std::string &text);
        virtual ParseError getError();

};

#endif // BASECHECKER_H
