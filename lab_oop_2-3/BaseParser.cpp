#include "BaseParser.h"
#include <string>

using namespace std;

BaseParser::BaseParser()
{

}

BaseParser::~BaseParser()
{

}

void BaseParser::setText(vector<string> &text)
{
    this->_text = "";

    size_t size = text.size();
    for(size_t i = 0; i < size; i++)
        this->_text += text[i];
}

void BaseParser::setText(string &text)
{
    this->_text = text;
}

void BaseParser::check(string &text)
{
    setText(text);
    check();
}

void BaseParser::check(vector<string> &text)
{
    setText(text);
    check();
}

ParseError BaseParser::getError()
{
    return _error;
}
