#include "CheckManager.h"

#include <string>
using namespace std;

bool CheckManager::isGap(char c)
{
    return (c == ' ' || c == '\n' || c == '\r' || c == '\t');
}

bool CheckManager::isQuote(char c)
{
    return (c == '"');
}

bool CheckManager::isOpeningBracket(char c)
{
    return (isQuote(c) || c == '[' || c == '{');
}

bool CheckManager::isClosingBracket(char c)
{
    return (isQuote(c) || c == ']' || c == '}');
}

bool CheckManager::isColon(char c)
{
    return (c == getColon());
}

char CheckManager::getClosingBracket(char c)
{
    if(!isOpeningBracket(c))
        return 0;

    char result;

    switch(c)
    {
        case '[':
            result = ']';
        break;
        case '{':
            result = '}';
        break;
        case '"':
            result = '"';
        break;
    }
    return result;
}

char CheckManager::getColon()
{
    return ':';
}

char CheckManager::getComma()
{
    return ',';
}

bool CheckManager::isComma(char c)
{
    return (c == getComma());
}

bool CheckManager::isNumber(char c)
{
    return ((c >= '0' && c <= '9') || c == '-' || c == '.');
}
