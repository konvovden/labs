#ifndef JSONCHECKER_H
#define JSONCHECKER_H

#include "BaseParser.h"

class JSONParser : public BaseParser
{
    public:
        void check() override;
};

#endif // JSONCHECKER_H
