#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "qfiledialog.h"
#include <QStandardItemModel>
#include <QMessageBox>
#include <QGraphicsOpacityEffect>
#include <QPicture>
#include <QPainter>

#include "Facade.h"
#include "JSONParser.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _facade = new Facade(new JSONParser());
}

MainWindow::~MainWindow()
{
    delete _facade;
    delete ui;
}



void MainWindow::on_ButtonStartCheck_clicked()
{
    QString str = ui->textEdit->toPlainText();

    _facade->loadData(str.toStdString());
    _facade->checkData();
    if(_facade->isError())
    {
        unsigned int pos = _facade->getErrorPos();
        ui->LabelErrorText->setText(QString::fromStdString(_facade->getErrorMessage()));
        ui->LabelErrorSymbol->setText(QString::number(pos));
        unsigned int length = ui->textEdit->toPlainText().length();
        if(pos >= length - 1)
            pos -= 2;
        setHighlight(pos + 1, pos + 2);
    }
    else
    {
        clearHighlight();
        ui->LabelErrorText->setText("No error");
        ui->LabelErrorSymbol->setText("");
    }
}

void MainWindow::on_ButtonChooseFile_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Open File", "", "JSON (*.json)");
    if(!file.length())
        return;

    _facade->setFilePath(file.toStdString());
    ui->label->setText("Файл: " + file.right(file.length() - file.lastIndexOf('/') - 1));
}

void MainWindow::on_ButtonLoadData_clicked()
{
    if(!_facade->getFilePath().length())
        return ;

    _facade->loadDataFromFile();
    ui->textEdit->setText(QString::fromStdString(_facade->getDataAsString()));
}



void MainWindow::on_pushButton_clicked()
{

}


void MainWindow::clearHighlight()
{
    QTextCursor cursor=ui->textEdit->textCursor();
    cursor.setPosition(0);
    cursor.setPosition(ui->textEdit->toPlainText().length(), QTextCursor::KeepAnchor);
    QTextCharFormat charFormat;
    cursor.setCharFormat(charFormat);
}

void MainWindow::setHighlight(int pos1, int pos2)
{
    QTextCursor cursor=ui->textEdit->textCursor();
    cursor.setPosition(pos1);
    cursor.setPosition(pos2, QTextCursor::KeepAnchor);
    QTextCharFormat charFormat = cursor.charFormat();
    charFormat.setBackground(QColor(150, 0, 0, 150));
    cursor.setCharFormat(charFormat);
}
