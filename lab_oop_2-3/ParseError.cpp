#include "ParseError.h"

using namespace std;

ParseError::ParseError(): _isEmpty(true), _text(""), _charNum(0)
{

}

ParseError::ParseError(string text, int charNum): _isEmpty(false), _text(text), _charNum(charNum)
{

}

bool ParseError::isEmpty()
{
    return _isEmpty;
}

string ParseError::getText()
{
    return _text;
}

int ParseError::getCharNum()
{
    return _charNum;
}

void ParseError::clear()
{
    _isEmpty = true;
    _text = "";
    _charNum = 0;
}

void ParseError::set(string text, int charNum)
{
    _isEmpty = false;
    _text = text;
    _charNum = charNum;
}
