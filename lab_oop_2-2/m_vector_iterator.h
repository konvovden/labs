#pragma once

#include <iterator>

template <typename T>
class m_vector;

template <typename T>
class m_vector_iterator: public std::iterator<std::input_iterator_tag, T>
{
    friend class m_vector<T>;
    private:
        m_vector_iterator(T *p);

    public:
        m_vector_iterator(const m_vector_iterator &it);

        bool operator!=(m_vector_iterator const& other) const;
        bool operator==(m_vector_iterator const& other) const;

        typename m_vector_iterator::reference operator*() const;
        m_vector_iterator& operator++();

    private:
        T* p;
};

template <typename T>
m_vector_iterator<T>::m_vector_iterator(T *p) : p(p)
{
}


template <typename T>
m_vector_iterator<T>::m_vector_iterator(const m_vector_iterator &it): p(it.p)
{
}

template <typename T>
bool m_vector_iterator<T>::operator!=(m_vector_iterator const& other) const
{
    return p != other.p;
}

template <typename T>
bool m_vector_iterator<T>::operator==(m_vector_iterator const& other) const
{
    return p == other.p;
}

template <typename T>
typename m_vector_iterator<T>::reference m_vector_iterator<T>::operator*() const
{
    return *p;
}

template <typename T>
m_vector_iterator<T> &m_vector_iterator<T>::operator++()
{
    ++p;
    return *this;
}

