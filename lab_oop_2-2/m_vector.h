#pragma once

#include <memory>
#include <initializer_list>
#include <m_vector_iterator.h>


template <typename T>
class m_vector
{
    public:
        m_vector(int length);
        m_vector(m_vector<T> &vect);
        explicit m_vector(std::initializer_list<T> lst);
        ~m_vector();

        m_vector<T> &operator = (const m_vector<T> &lst);

        size_t get_length();

        void set_elem(unsigned int index, const T &elem);
        T& get_elem(unsigned int index);
        T* to_array();
        T& operator[](unsigned int index);

        template <typename _T>
        friend std::ostream& operator <<(std::ostream& os, m_vector<_T>& lst);

        m_vector<T>& operator +=(const m_vector<T> &vect);
        m_vector<T>& operator -=(const m_vector<T> &vect);
        m_vector<T>& operator *=(const T &val);
        m_vector<T>& operator /=(const T &val);

        template <typename _T>
        friend m_vector<_T> operator +(m_vector<_T> &v1, m_vector<_T> &v2);

        template <typename _T>
        friend m_vector<_T> operator -(m_vector<_T> &v1, m_vector<_T> &v2);

        template<typename _T>
        friend m_vector<_T> operator *(m_vector<_T>& v1,  _T& v2);

        template<typename _T>
        friend m_vector<_T> operator /(m_vector<_T>& v1, _T& v2);



        m_vector_iterator<T> begin();
        m_vector_iterator<T> end();

    private:
        T * data { nullptr };
        const size_t size;

};

template <typename T>
m_vector<T>::m_vector(int length): data(new T[length]), size(length)
{
}

template <typename T>
m_vector<T>::m_vector(m_vector<T>& vect): m_vector<T>(vect.get_length())
{
    for(size_t i = 0; i < size; i++)
        data[i] = vect[i];
}

template <typename T>
m_vector<T>::m_vector(std::initializer_list<T> lst) : m_vector<T>(lst.size())
{
    int i = 0;
    for(auto &e : lst)
        data[i++] = e;
}

template <typename T>
m_vector<T>::~m_vector<T>()
{
    delete data;
}

template <typename T>
m_vector<T> &m_vector<T>::operator=(const m_vector<T> &lst)
{
    for(size_t i = 0; i < size; i++)
        data[i] = lst.data[i];

    return *this;
}

template <typename T>
size_t m_vector<T>::get_length()
{
    return size;
}

template <typename T>
void m_vector<T>::set_elem(unsigned int index, const T &elem)
{
    if(index < size)
        data[index] = elem;
}

template <typename T>
T& m_vector<T>::get_elem(unsigned int index)
{
    return data[index];
}

template <typename T>
T* m_vector<T>::to_array()
{
    return data;
}

template <typename T>
T& m_vector<T>::operator[](unsigned int index)
{
    return data[index];
}

template <typename T>
m_vector<T>& m_vector<T>::operator+=(const m_vector<T> &vect)
{
    for(size_t i = 0; i < size; i++)
        data[i] += vect.data[i];

    return *this;
}

template <typename T>
m_vector<T>& m_vector<T>::operator-=(const m_vector<T> &vect)
{
    for(size_t i = 0; i < size; i++)
        data[i] -= vect.data[i];

    return *this;
}

template <typename T>
m_vector<T>& m_vector<T>::operator*=(const T &val)
{
    for(size_t i = 0; i < size; i++)
        data[i] *= val;

    return *this;
}

template <typename T>
m_vector<T>& m_vector<T>::operator/=(const T &val)
{
    for(size_t i = 0; i < size; i++)
        data[i] /= val;

    return *this;
}

template <typename _T>
m_vector<_T> operator+(m_vector<_T> &v1, m_vector<_T> &v2)
{
    m_vector<_T> result(v1);
    for(size_t i = 0; i < result.size; i++)
        result.data[i] += v2.data[i];

    return result;
}

template <typename _T>
m_vector<_T> operator-(m_vector<_T> &v1, m_vector<_T> &v2)
{
    m_vector<_T> result(v1);
    for(size_t i = 0; i < result.size; i++)
        result.data[i] -= v2.data[i];

    return result;
}

template <typename _T>
m_vector<_T> operator*(m_vector<_T> &v1, _T &v2)
{
    m_vector<_T> result(v1);
    for(size_t i = 0; i < result.size; i++)
        result.data[i] *= v2;

    return result;
}

template <typename _T>
m_vector<_T> operator/(m_vector<_T> &v1, _T &v2)
{
    m_vector<_T> result(v1);
    for(size_t i = 0; i < result.size; i++)
        result.data[i] /= v2;

    return result;
}

template <typename _T>
std::ostream& operator <<(std::ostream& os, m_vector<_T>& lst)
{
    size_t size = lst.get_length();
    for(size_t i = 0; i < size; i++)
        os << lst.data[i] << ' ';

    return os;
}


template <typename T>
m_vector_iterator<T> m_vector<T>::begin()
{
    return m_vector_iterator<T>(data);
}

template <typename T>
m_vector_iterator<T> m_vector<T>::end()
{
    return m_vector_iterator<T>(data + size);
}
