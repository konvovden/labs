#include "m_vector.h"
#include <iostream>

using namespace std;

int main()
{
    cout << "Constructor by length: " << '\n';
    m_vector<int> constructor_length(3);
    cout << constructor_length << '\n';

    cout << "Constructor initializer list: " << '\n';
    m_vector<int> constructor_il{1, 2, 3};
    cout << constructor_il << '\n';

    cout << "Constructor copy: " << '\n';
    m_vector<int> constructor_copy(constructor_il);
    cout << constructor_copy << '\n';
    cout << '\n';

    cout << "Operator =: " << '\n';
    m_vector<int> operator_eq(3);
    operator_eq = constructor_copy;
    cout << operator_eq << '\n';
    cout << '\n';

    cout << "get_length: " << '\n';
    cout << operator_eq.get_length() << '\n';
    cout << '\n';


    cout << "set_elem: " << '\n';
    cout << operator_eq << '\n';
    operator_eq.set_elem(1, 10);
    cout << operator_eq << '\n';
    cout << '\n';

    cout << "get_elem: " << '\n';
    cout << operator_eq.get_elem(2) << '\n';
    cout << '\n';

    cout << "to_array: " << '\n';
    cout << operator_eq << '\n';
    size_t size = operator_eq.get_length();
    int *array = new int[size];
    array = operator_eq.to_array();
    for(size_t i = 0; i < size; i++)
        cout << array[i] << ' ';
    cout << '\n';

    cout << "operator[]: " << '\n';
    cout << operator_eq << '\n';
    operator_eq[1] = 20;
    cout << operator_eq << '\n';
    cout << '\n';

    cout << "operator +=: " << '\n';
    m_vector<int> vec1{9, 8, 7, 6};
    cout << vec1 << '\n';
    cout << '+' << '\n';
    m_vector<int> vec2{1, 2, 3, 4};
    cout << vec2 << '\n';
    cout << '=' << '\n';
    vec1 += vec2;
    cout << vec1 << '\n';
    cout << '\n';

    cout << "operator -=: " << '\n';
    cout << vec1 << '\n';
    cout << '-' << '\n';
    cout << vec2 << '\n';
    cout << '=' << '\n';
    vec1 -= vec2;
    cout << vec1 << '\n';
    cout << '\n';

    int a = 3;

    cout << "operator *=: " << '\n';
    cout << vec1 << '\n';
    cout << '*' << '\n';
    cout << a << '\n';
    cout << '=' << '\n';
    vec1 *= a;
    cout << vec1 << '\n';
    cout << '\n';

    cout << "operator /=: " << '\n';
    cout << vec1 << '\n';
    cout << '/' << '\n';
    cout << a << '\n';
    cout << '=' << '\n';
    vec1 /= a;
    cout << vec1 << '\n';
    cout << '\n';

    m_vector<int> vec3(vec1.get_length());

    cout << "operator +: " << '\n';
    cout << vec1 << '\n';
    cout << '+' << '\n';
    cout << vec2 << '\n';
    cout << '=' << '\n';
    vec3 = vec1 + vec2;
    cout << vec3 << '\n';
    cout << '\n';

    cout << "operator -: " << '\n';
    cout << vec1 << '\n';
    cout << '-' << '\n';
    cout << vec2 << '\n';
    cout << '=' << '\n';
    vec3 = vec1 - vec2;
    cout << vec3 << '\n';
    cout << '\n';

    cout << "operator *: " << '\n';
    cout << vec2 << '\n';
    cout << '*' << '\n';
    cout << a << '\n';
    cout << '=' << '\n';
    vec3 = vec2 * a;
    cout << vec3 << '\n';
    cout << '\n';

    cout << "operator /: " << '\n';
    cout << vec3 << '\n';
    cout << '/' << '\n';
    cout << a << '\n';
    cout << '=' << '\n';
    vec3 = vec3 / a;
    cout << vec3 << '\n';
    cout << '\n';

    cout << "iterator: " << '\n';
    int num = 0;

    m_vector_iterator<int> iter = vec3.begin();
    while(iter != vec3.end())
    {
        cout << "[" << num++ << "] = " << *iter << '\n';
        ++iter;
    }
}
