﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Logic.Enums;

namespace Calculator.Logic.Interfaces
{
    public interface ICalculatorManager
    {
        string Clear();
        string ApplyAction(CalculatorActionType action);
        string InputNumber(int num);
        string DelNumber();
        string GetMemory();
        string ClearMemory();
        string PlusMemory();
        string MinusMemory();
    }
}
