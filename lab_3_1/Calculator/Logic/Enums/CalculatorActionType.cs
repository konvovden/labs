﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic.Enums
{
    public enum CalculatorActionType
    {
        None = 0,
        Plus = 1,
        Minus = 2,
        Divide = 3,
        Mult = 4,
        Power = 5,
        Sqrt = 6,
        DivX = 7,
        Sin = 8,
        Cos = 9,
        Tg = 10,
        PlusMinus = 11,
        Dot = 12,
        Equally = 13
    }
}
