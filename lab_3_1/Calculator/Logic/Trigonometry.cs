﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.Logic
{
    public static class Trigonometry
    {
        public static double AngleToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
        public static double RadiansToAngle(double radians)
        {
            return (180 / Math.PI) * radians;
        }
    }
}
