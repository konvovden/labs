﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.Logic.Enums;
using Calculator.Logic.Interfaces;

namespace Calculator.Logic
{
    public class CalculatorManager : ICalculatorManager
    {
        private string _screenValue;
        private double _firstValue;
        private double? _memoryValue = null;
        private CalculatorActionType _currentAction;
        private readonly uint _resultLabelMaxLength = 15;
        private bool _clearInput;
        private bool _error;

        public CalculatorManager()
        {
            Clear();
        }

        public CalculatorManager(uint resultLabelMaxLength) : base()
        {
            _resultLabelMaxLength = resultLabelMaxLength;
        }

        private void SetError()
        {
            _screenValue = "ERROR";
            _error = true;
        }

        public string ApplyAction(CalculatorActionType action)
        {
            if (_error)
                return _screenValue;
            switch (action)
            {
                case CalculatorActionType.None:
                    break;
                case CalculatorActionType.Plus:
                case CalculatorActionType.Minus:
                case CalculatorActionType.Divide:
                case CalculatorActionType.Mult:
                case CalculatorActionType.Power:
                    if (_currentAction == CalculatorActionType.None)
                        _firstValue = Convert.ToDouble(_screenValue);
                    _currentAction = action;
                    _screenValue = "";
                    break;
                case CalculatorActionType.Sqrt:
                    _screenValue = Convert.ToString(Math.Sqrt(Convert.ToDouble(_screenValue)));
                    _clearInput = true;
                    break;
                case CalculatorActionType.DivX:
                    if (Convert.ToDouble(_screenValue) != 0.0)
                    {
                        _screenValue = Convert.ToString(1 / Convert.ToDouble(_screenValue));
                        _clearInput = true;
                    }
                    else
                        SetError();
                    break;
                case CalculatorActionType.Sin:
                    _screenValue = Convert.ToString(Math.Sin(Trigonometry.AngleToRadians(Convert.ToDouble(_screenValue))));
                    _clearInput = true;
                    break;
                case CalculatorActionType.Cos:
                    _screenValue = Convert.ToString(Math.Cos(Trigonometry.AngleToRadians(Convert.ToDouble(_screenValue))));
                    _clearInput = true;
                    break;
                case CalculatorActionType.Tg:
                    {
                        double result = Math.Tan(Trigonometry.AngleToRadians(Convert.ToDouble(_screenValue)));
                        if (Double.IsNaN(result))
                        {
                            _screenValue = Convert.ToString(result);
                            _clearInput = true;
                        }
                        else
                            SetError();
                        break;
                    }
                case CalculatorActionType.PlusMinus:
                    double value = Convert.ToDouble(_screenValue);

                    if (value > 0 && _screenValue.Length < _resultLabelMaxLength)
                        _screenValue = "-" + _screenValue;
                    else if (value < 0)
                        _screenValue = _screenValue.Substring(1, _screenValue.Length - 1);
                    break;
                case CalculatorActionType.Dot:
                    if (!_screenValue.Contains(',') && _screenValue.Length < _resultLabelMaxLength - 1)
                    {
                        if (_clearInput || _error)
                        {
                            _screenValue = "";
                            _clearInput = false;
                            _error = false;
                        }   
                        if (_screenValue.Length < 1)
                            _screenValue = "0";
                        _screenValue += ',';
                    }
                    break;
                case CalculatorActionType.Equally:
                    if (_screenValue.Length > 0)
                    {
                        double? result = null;
                        switch (_currentAction)
                        {
                            case CalculatorActionType.Plus:
                                result = _firstValue + Convert.ToDouble(_screenValue);
                                break;
                            case CalculatorActionType.Minus:
                                result = _firstValue - Convert.ToDouble(_screenValue);
                                break;
                            case CalculatorActionType.Divide:
                                if(Convert.ToDouble(_screenValue) != 0.0)
                                    result = _firstValue / Convert.ToDouble(_screenValue);
                                break;
                            case CalculatorActionType.Mult:
                                result = _firstValue * Convert.ToDouble(_screenValue);
                                break;
                            case CalculatorActionType.Power:
                                result = Math.Pow(_firstValue, Convert.ToDouble(_screenValue));
                                break;
                            default:
                                result = Convert.ToDouble(_screenValue);
                                break;
                        }
                        if (result == null)
                            SetError();
                        else
                            _screenValue = Convert.ToString(result);

                        _currentAction = CalculatorActionType.None;
                        _firstValue = 0;
                        _clearInput = true;
                    }
                    break;
            }

            return _screenValue;
        }

        public string Clear()
        {
            _screenValue = "0";
            _clearInput = false;
            _error = false;
            return _screenValue;
        }

        public string DelNumber()
        {
            int length = _screenValue.Length;
            if (length < 2 || _clearInput || _error)
            {
                _screenValue = "0";
                _clearInput = false;
                _error = false;
            }
            else if (length >= 2 && _screenValue[length - 2] == '.')
                _screenValue = _screenValue.Substring(0, length - 2);
            else
                _screenValue = _screenValue.Substring(0, length - 1);

            return _screenValue;
        }

        public string InputNumber(int num)
        {
            if(_clearInput || _error)
            {
                _clearInput = false;
                _error = false;
                _screenValue = "";
            }
            if(_screenValue.Length < _resultLabelMaxLength)
            {
                if (_screenValue.Contains('.'))
                    _screenValue += Convert.ToString(num);
                else
                    _screenValue = Convert.ToString(Convert.ToDouble(_screenValue + Convert.ToString(num)));
            }
            
            return _screenValue;
        }

        public string ClearMemory()
        {
            _memoryValue = null;
            return "";
        }
        public string MinusMemory()
        {
            if (_error)
            {
                if (_memoryValue == null)
                    return "";
                return "Mem: " + Convert.ToString(_memoryValue);
            }
            if (_memoryValue == null)
                _memoryValue = 0.0;
            _memoryValue -= Convert.ToDouble(_screenValue);

            return "Mem: " + Convert.ToString(_memoryValue);
        }

        public string PlusMemory()
        {
            if (_error)
            {
                if (_memoryValue == null)
                    return "";
                return "Mem: " + Convert.ToString(_memoryValue);
            }
            if (_memoryValue == null)
                _memoryValue = 0.0;
            _memoryValue += Convert.ToDouble(_screenValue);

            return "Mem: " + Convert.ToString(_memoryValue);
        }

        public string GetMemory()
        {
            if(_memoryValue != null)
            {
                _screenValue = Convert.ToString(_memoryValue);
                _error = false;
                _clearInput = false;
            }

            return _screenValue;
        }
    }
}
