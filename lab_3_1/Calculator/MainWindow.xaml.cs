﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Calculator.Logic.Interfaces;
using Calculator.Logic.Enums;
using Calculator.Logic;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ICalculatorManager _calcManager;
        public MainWindow()
        {
            InitializeComponent();
            _calcManager = new CalculatorManager();
        }
        private void SetResultLabelText(string text)
        {
            if(text != null)
                ResultLabel.Text = text;
        }
        private void SetMemoryLabelText(string text)
        {
            if (text != null)
                MemoryLabel.Text = text;
        }
        private void MemoryButtonClick(object sender, EventArgs e)
        {
            string buttonText = (string)((Button)sender).Content;
            switch(buttonText)
            {
                case "M+":
                    SetMemoryLabelText(_calcManager.PlusMemory());
                    break;
                case "M-":
                    SetMemoryLabelText(_calcManager.MinusMemory());
                    break;
                case "MR":
                    SetResultLabelText(_calcManager.GetMemory());
                    break;
                case "MC":
                    SetMemoryLabelText(_calcManager.ClearMemory());
                    break;
            }
        }

        private void NumberButtonClick(object sender, EventArgs e)
        {
            string buttonText = (string)((Button)sender).Content;
            int number;
            if (Int32.TryParse(buttonText, out number))
            {
                string result = _calcManager.InputNumber(number);
                SetResultLabelText(result);
            }
        }

        private void ClearButtonClick(object sender, EventArgs e)
        {
            string result = _calcManager.Clear();
            SetResultLabelText(result);
        }

        private void DelButtonClick(object sender, EventArgs e)
        {
            string result = _calcManager.DelNumber();
            SetResultLabelText(result);
        }

        private void ActionButtonClick(object sender, EventArgs e)
        {
            string buttonText = (string)((Button)sender).Content;

            CalculatorActionType action = CalculatorActionType.None;

            switch(buttonText)
            {
                case "÷":
                    action = CalculatorActionType.Divide;
                    break;
                case "×":
                    action = CalculatorActionType.Mult;
                    break;
                case "-":
                    action = CalculatorActionType.Minus;
                    break;
                case "+":
                    action = CalculatorActionType.Plus;
                    break;
                case "=":
                    action = CalculatorActionType.Equally;
                    break;
                case ".":
                    action = CalculatorActionType.Dot;
                    break;
                case "±":
                    action = CalculatorActionType.PlusMinus;
                    break;
                case "pow":
                    action = CalculatorActionType.Power;
                    break;
                case "√":
                    action = CalculatorActionType.Sqrt;
                    break;
                case "1/x":
                    action = CalculatorActionType.DivX;
                    break;
                case "sin":
                    action = CalculatorActionType.Sin;
                    break;
                case "tg":
                    action = CalculatorActionType.Tg;
                    break;
                case "cos":
                    action = CalculatorActionType.Cos;
                    break;
            }

            string result = _calcManager.ApplyAction(action);
            SetResultLabelText(result);
        }
    }
}
