﻿using System;
using System.Collections;
using System.Collections.Generic;
using MathLib.Classes;
using Xunit;

namespace MathLib.Tests
{
    public class MathVectorTests
    {
        #region SumNumberTests
        [Fact]
        public void SumNumberTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.SumNumber(0));
        }

        [Fact]
        public void SumNumberTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector.SumNumber(1));
        }

        [Fact]
        public void SumNumberTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector.SumNumber(-1));
        }
        #endregion

        #region MultiplyNumberTests
        [Fact]
        public void MultiplyNumberTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 0, 0, 0 }, vector.MultiplyNumber(0));
        }

        [Fact]
        public void MultiplyNumberTestPos1()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.MultiplyNumber(1));
        }

        [Fact]
        public void MultiplyNumberTestPos2()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -2, 0, 2 }, vector.MultiplyNumber(2));
        }

        [Fact]
        public void MultiplyNumberTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector.MultiplyNumber(-1));
        }
        #endregion

        #region SumTests
        [Fact]
        public void SumTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.Sum(testVector));
        }

        [Fact]
        public void SumTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector.Sum(testVector));
        }

        [Fact]
        public void SumTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector.Sum(testVector));
        }
        #endregion

        #region MultiplyTests
        [Fact]
        public void MultiplyTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { 0, 0, 0 }, vector.Multiply(testVector));
        }

        [Fact]
        public void MultiplyTestPos1()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.Multiply(testVector));
        }

        [Fact]
        public void MultiplyTestPos2()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 2, 2 };

            Assert.Equal(new MathVector { -2, 0, 2 }, vector.Multiply(testVector));
        }

        [Fact]
        public void MultiplyTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector.Multiply(testVector));
        }
        #endregion

        #region MinusTests
        [Fact]
        public void MinusTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.Minus(testVector));
        }

        [Fact]
        public void MinusTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector.Minus(testVector));
        }

        [Fact]
        public void MinusTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector.Minus(testVector));
        }
        #endregion

        #region DivTests
        [Fact]
        public void DivTestPos1()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector.Div(testVector));
        }

        [Fact]
        public void DivTestPos2()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 2, 2 };

            Assert.Equal(new MathVector { -0.5, 0, 0.5 }, vector.Div(testVector));
        }

        [Fact]
        public void DivTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector.Div(testVector));
        }
        #endregion

        #region ScalarMultiplyTests
        [Fact]
        public void ScalarMultiplyTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(0.0, vector.ScalarMultiply(testVector));
        }

        [Fact]
        public void ScalarMultiplyTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 1, 1 };

            Assert.Equal(-1.0, vector.ScalarMultiply(testVector));
        }

        [Fact]
        public void ScalarMultiplyTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -2, -1, -1 };

            Assert.Equal(1.0, vector.ScalarMultiply(testVector));
        }
        #endregion

        #region CalcDistanceTests
        [Fact]
        public void CalcDistanceTestMix()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, 0, 1 };

            Assert.Equal(0.0, vector.CalcDistance(testVector));
        }

        [Fact]
        public void CalcDistanceTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(Math.Sqrt(2), vector.CalcDistance(testVector));
        }

        [Fact]
        public void CalcDistanceTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(Math.Sqrt(5), vector.CalcDistance(testVector));
        }

        [Fact]
        public void CalcDistanceTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(Math.Sqrt(5), vector.CalcDistance(testVector));
        }
        #endregion

        #region CalcAverageTests
        [Fact]
        public void CalcAverageZero()
        {
            var vector = new MathVector { 0, 0, 0 };

            Assert.Equal(0.0, vector.CalcAverage());
        }

        [Fact]
        public void CalcAveragePos()
        {
            var vector = new MathVector { 1, 1, 1 };

            Assert.Equal(1.0, vector.CalcAverage());
        }

        [Fact]
        public void CalcAverageNeg()
        {
            var vector = new MathVector { -1, -1, -1 };

            Assert.Equal(-1.0, vector.CalcAverage());
        }

        [Fact]
        public void CalcAverageMix()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(0.0, vector.CalcAverage());
        }
        #endregion

        #region OperatorPlusTests
        [Fact]
        public void OperatorPlusTestZeroVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector + testVector);
        }

        [Fact]
        public void OperatorPlusTestPosVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector + testVector);
        }

        [Fact]
        public void OperatorPlusTestNegVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector + testVector);
        }

        [Fact]
        public void OperatorPlusTestZeroNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector + 0);
        }

        [Fact]
        public void OperatorPlusTestPosNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector + 1);
        }

        [Fact]
        public void OperatorPlusTestNegNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector + (-1));
        }
        #endregion

        #region OperatorMinusTests
        [Fact]
        public void OperatorMinusTestZeroVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector - testVector);
        }

        [Fact]
        public void OperatorMinusTestPosVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector - testVector);
        }

        [Fact]
        public void OperatorMinusTestNegVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector - testVector);
        }

        [Fact]
        public void OperatorMinusTestZeroNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector - 0);
        }

        [Fact]
        public void OperatorMinusTestPosNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -2, -1, 0 }, vector - 1);
        }

        [Fact]
        public void OperatorMinusTestNegNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 0, 1, 2 }, vector - (-1));
        }
        #endregion

        #region OperatorMultTests
        [Fact]
        public void OperatorMultTestZeroNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 0, 0, 0 }, vector * 0);
        }

        [Fact]
        public void OperatorMultTestPosNum1()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector * 1);
        }

        [Fact]
        public void OperatorMultTestPosNum2()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -2, 0, 2 }, vector * 2);
        }

        [Fact]
        public void OperatorMultTestNegNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector * (-1));
        }

        [Fact]
        public void OperatorMultTestZeroVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(new MathVector { 0, 0, 0 }, vector * testVector);
        }

        [Fact]
        public void OperatorMultTestPosVec1()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector * testVector);
        }

        [Fact]
        public void OperatorMultTestPosVec2()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 2, 2 };

            Assert.Equal(new MathVector { -2, 0, 2 }, vector * testVector);
        }

        [Fact]
        public void OperatorMultTestNegVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector * testVector);
        }
        #endregion

        #region OperatorDivTests
        [Fact]
        public void OperatorDivTestPosNum1()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector / 1);
        }

        [Fact]
        public void OperatorDivTestPosNum2()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { -0.5, 0, 0.5 }, vector / 2);
        }

        [Fact]
        public void OperatorDivTestNegNum()
        {
            var vector = new MathVector { -1, 0, 1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector / (-1));
        }

        [Fact]
        public void OperatorDivTestPosVec1()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 1, 1, 1 };

            Assert.Equal(new MathVector { -1, 0, 1 }, vector / testVector);
        }

        [Fact]
        public void OperatorDivTestPosVec2()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 2, 2 };

            Assert.Equal(new MathVector { -0.5, 0, 0.5 }, vector / testVector);
        }

        [Fact]
        public void OperatorDivTestNegVec()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -1, -1, -1 };

            Assert.Equal(new MathVector { 1, 0, -1 }, vector / testVector);
        }
        #endregion

        #region OperatorProcentTests
        [Fact]
        public void OperatorProcentTestZero()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 0, 0, 0 };

            Assert.Equal(0.0, vector.ScalarMultiply(testVector));
        }

        [Fact]
        public void OperatorProcentTestPos()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { 2, 1, 1 };

            Assert.Equal(-1.0, vector.ScalarMultiply(testVector));
        }

        [Fact]
        public void OperatorProcentTestNeg()
        {
            var vector = new MathVector { -1, 0, 1 };
            var testVector = new MathVector { -2, -1, -1 };

            Assert.Equal(1.0, vector.ScalarMultiply(testVector));
        }
        #endregion

    }
}