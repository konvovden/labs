﻿using MathLib.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathLib.Classes
{
    /// <inheritdoc cref="IMathVector"/>
    public class MathVector : IMathVector
    {
        private List<double> Elements = new List<double>();
        public double this[int i] { get => Elements[i]; set => Elements[i] = value; }

        public int Dimensions => Elements.Count();

        public double Length 
        {
            get
            {
                double result = 0.0;
                foreach (double v in Elements)
                    result += Math.Pow(v, 2);
                return Math.Sqrt(result);
            }
        }

        public MathVector()
        {
        }

        public MathVector(IEnumerable<double> elems)
        {
            Elements = new List<double>(elems);
        }

        public MathVector(IMathVector vector)
        {
            foreach (double v in vector)
                Elements.Add(v);
        }

        public void Add(double value)
        {
            Elements.Add(value);
        }

        public double CalcDistance(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            double result = 0.0;

            for (int i = 0; i < Dimensions; i++)
                result += Math.Pow(this[i] - vector[i], 2);

            return Math.Sqrt(result);
        }

        public IEnumerator GetEnumerator()
        {
            return Elements.GetEnumerator();
        }

        public IMathVector Multiply(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] *= vector[i];

            return result;
        }

        public IMathVector MultiplyNumber(double number)
        {
            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] *= number;

            return result;
        }

        public double ScalarMultiply(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            double result = 0.0;
            for (int i = 0; i < Dimensions; i++)
                result += this[i] * vector[i];

            return result;
        }

        public IMathVector Sum(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] += vector[i];

            return result;
        }

        public IMathVector SumNumber(double number)
        {
            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] += number;

            return result;
        }

        public IMathVector Minus(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] -= vector[i];

            return result;
        }

        public IMathVector Div(IMathVector vector)
        {
            if (this.Dimensions != vector.Dimensions)
                throw new ArgumentException("Vectors` dimensions not equal.", "vector");

            IMathVector result = new MathVector(this);
            for (int i = 0; i < result.Dimensions; i++)
                result[i] /= vector[i];

            return result;
        }

        public double CalcAverage()
        {
            if (Dimensions == 0)
                return 0.0;

            double result = 0.0;

            foreach (double v in this)
                result += v;

            return result / Dimensions;
        }

        /// <summary>
        /// Покомпонентное сложение с числом.
        /// </summary>
        public static IMathVector operator +(MathVector left, double value)
        {
            return left.SumNumber(value);
        }

        /// <summary>
        /// Покомпонентное сложение с другим вектором.
        /// </summary>
        public static IMathVector operator +(MathVector left, MathVector right)
        {
            return left.Sum(right);
        }

        /// <summary>
        /// Покомпонентное вычитание с числом.
        /// </summary>
        public static IMathVector operator -(MathVector left, double value)
        {
            return left.SumNumber(-value);
        }

        /// <summary>
        /// Покомпонентное вычитание с другим вектором.
        /// </summary>
        public static IMathVector operator -(MathVector left, MathVector right)
        {
            return left.Minus(right);
        }

        /// <summary>
        /// Покомпонентное умножение с другим числом.
        /// </summary>
        public static IMathVector operator *(MathVector left, double value)
        {
            return left.MultiplyNumber(value);
        }

        /// <summary>
        /// Покомпонентное умножение с другим вектором.
        /// </summary>
        public static IMathVector operator *(MathVector left, MathVector right)
        {
            return left.Multiply(right);
        }

        /// <summary>
        /// Покомпонентное деление с числом.
        /// </summary>
        public static IMathVector operator /(MathVector left, double value)
        {
            return left.MultiplyNumber(1 / value);
        }

        /// <summary>
        /// Покомпонентное деление с другим вектором.
        /// </summary>
        public static IMathVector operator /(MathVector left, MathVector right)
        {
            return left.Div(right);
        }

        /// <summary>
        /// Скалярное умножение двух векторов.
        /// </summary>
        public static double operator %(MathVector left, MathVector right)
        {
            return left.ScalarMultiply(right);
        }
    }
}
