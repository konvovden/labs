﻿using MathLib.Classes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iris.Logic
{
    /// <summary>
    /// Manager for operations with data.
    /// </summary>
    public static class DataManager
    {
        private static readonly char _splitter = ',';
        private static readonly int _decimals = 2;
        /// <summary>
        /// Extracts graphics properties names from raw data.
        /// </summary>
        public static string[] GetGraphicsNames(string[] data)
        {
            return data[0].Split(_splitter);
        }

        /// <summary>
        /// Extracts graphics values from raw data.
        /// </summary>
        public static List<MathVector> GetGraphicsValues(string[] data)
        {
            var dataArray = CalcAverageDataArray(GetDataArray(data));
            var result = GetCartesianGraphicsValues(dataArray);
            result.Add(GetPieChartGraphicValues(dataArray));

            return result;
        }

        /// <summary>
        /// Extracts irises names from raw data.
        /// </summary>
        public static List<string> GetIrisNames(string[] data)
        {
            List<string> result = new List<string>();

            int lastElem = -1;
            for (int i = 1; i < data.Length; i++)
            {
                string[] tempData = data[i].Split(_splitter);
                if (lastElem == -1)
                    lastElem = tempData.Length - 1;
                else if (lastElem != tempData.Length - 1)
                    throw new FileFormatException("Not all file lines have the same elements amount!");

                result.Add(tempData[lastElem]);
            }

            return result.Distinct().ToList();
        }

        private static List<MathVector> GetCartesianGraphicsValues(List<MathVector> dataArray)
        {
            var result = new List<MathVector>();
            for (int i = 0; i < dataArray[0].Dimensions; i++)
            {
                result.Add(new MathVector());
                for (int j = 0; j < dataArray.Count; j++)
                    result[i].Add(Math.Round(dataArray[j][i], _decimals));
            }

            return result;
        }

        private static MathVector GetPieChartGraphicValues(List<MathVector> dataArray)
        {
            return new MathVector
                {
                    Math.Round(dataArray[0].CalcDistance(dataArray[1]), _decimals),
                    Math.Round(dataArray[0].CalcDistance(dataArray[2]), _decimals),
                    Math.Round(dataArray[1].CalcDistance(dataArray[2]), _decimals)
                };
        }

        private static List<List<MathVector>> GetDataArray(string[] data)
        {
            List<List<MathVector>> result = new List<List<MathVector>>();

            string currentName = "";
            int lastElem = -1;
            int irisNum = -1;
            for(int i = 1; i < data.Length; i++)
            {
                string[] tempData = data[i].Split(_splitter);
                if (lastElem == -1)
                    lastElem = tempData.Length - 1;
                else if (lastElem != tempData.Length - 1)
                    throw new FileFormatException("Not all file lines have the same elements amount!");

                if(currentName != tempData[lastElem])
                {
                    irisNum++;
                    result.Add(new List<MathVector>()) ;
                    for (int j = 0; j < lastElem; j++)
                        result[irisNum].Add(new MathVector());

                    currentName = tempData[lastElem];
                }

                AddListValues(result[irisNum], tempData, lastElem);
            }

            return result;
        }

        private static List<MathVector> CalcAverageDataArray(List<List<MathVector>> dataArray)
        {
            var result = new List<MathVector>();

            for (int i = 0; i < dataArray.Count; i++)
                result.Add(CalcAverageVector(dataArray[i]));

            return result;
        }

        private static MathVector CalcAverageVector(List<MathVector> list)
        {
            var result = new MathVector();

            for (int i = 0; i < list.Count; i++)
                result.Add(list[i].CalcAverage());

            return result;
        }

        private static void AddListValues(List<MathVector> list, string[] values, int lastElem)
        {
            for (int j = 0; j < lastElem; j++)
                list[j].Add(Double.Parse(values[j], CultureInfo.InvariantCulture));
        }
    }
}
