﻿using Iris.Commands;
using Iris.Logic;
using LiveCharts;
using LiveCharts.Wpf;
using MathLib.Classes;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Iris.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        /// <summary>
        /// Names of properties for graphics.
        /// </summary>
        public string[] GraphicsNames
        {
            get => _graphicsNames;
            set
            {
                _graphicsNames = value;
                NotifyPropertyChanged();
            }
        }
        private string[] _graphicsNames;

        
        /// <summary>
        /// Collections with values for graphics.
        /// </summary>
        public SeriesCollection[] GraphicsSeriesCollections
        {
            get => _graphicsSeriesCollections;
            set
            {
                _graphicsSeriesCollections = value;
                NotifyPropertyChanged();
            }
        }
        private SeriesCollection[] _graphicsSeriesCollections;

        /// <summary>
        /// Path to selected file.
        /// </summary>
        public string FilePath { get; set; } = "";
        
        /// <summary>
        /// Name of selected file.
        /// </summary>
        public string FileName 
        {
            get => "Filename: " + _fileName;
            set
            {
                _fileName = value;
                NotifyPropertyChanged();
            }
        }
        private string _fileName = "None";

        /// <summary>
        /// Loading file status.
        /// </summary>
        public bool FileLoaded
        {
            get => _fileLoaded;
            set
            {
                _fileLoaded = value;
                NotifyPropertyChanged();
            }
        }
        private bool _fileLoaded = false;

        /// <summary>
        /// Text of error while file loading.
        /// </summary>
        public string ErrorText
        {
            get => _errorText;
            set
            {
                _errorText = value;
                NotifyPropertyChanged();
            }
        }
        private string _errorText = "";

        /// <summary>
        /// Command to choose file.
        /// </summary>
        public ICommand ChooseFileCommand
        {
            get
            {
                if (_chooseFileCommand == null)
                    _chooseFileCommand = new DelegateCommand(o => ChooseFile());

                return _chooseFileCommand;
            }
        }
        private DelegateCommand _chooseFileCommand;

        /// <summary>
        /// Command to load choosen file.
        /// </summary>
        public ICommand LoadFileCommand
        {
            get 
            {
                if (_loadFileCommand == null)
                    _loadFileCommand = new DelegateCommand(o => LoadFile());

                return _loadFileCommand;
            }
        }
        private DelegateCommand _loadFileCommand;

        private void ChooseFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV Files (*.csv)|*.csv";
            if(dialog.ShowDialog() == true)
            {
                FilePath = dialog.FileName;
                FileName = dialog.SafeFileName;
            }
        }

        private void LoadFile()
        {
            FileLoaded = false;
            if (!File.Exists(FilePath))
                return;

            string[] data = File.ReadAllLines(FilePath);
            try
            {
                GraphicsNames = DataManager.GetGraphicsNames(data);
                GraphicsSeriesCollections = ConvertToSeriesCollections(DataManager.GetIrisNames(data), DataManager.GetGraphicsValues(data));
                FileLoaded = true;
            } 
            catch(FileFormatException e)
            {
                ErrorText = e.Message;
            }
        }

        private static SeriesCollection[] ConvertToSeriesCollections(List<string> names, List<MathVector> values)
        {
            SeriesCollection[] result = new SeriesCollection[5];
            for (int i = 0; i < values.Count - 1; i++)
                result[i] = ConvertToChartSeriesCollection(names, values[i]);

            result[values.Count - 1]  = ConvertToPieSeriesCollection(names, values[values.Count - 1]);

            return result;
        }

        private static SeriesCollection ConvertToChartSeriesCollection(List<string> names, MathVector values)
        {
            var result = new SeriesCollection();
            for (int j = 0; j < values.Dimensions; j++)
            {
                result.Add(
                    new ColumnSeries
                    {
                        Title = names[j],
                        Values = new ChartValues<double>() { values[j] }
                    });
            }
            return result;
        }

        private static SeriesCollection ConvertToPieSeriesCollection(List<string> names, MathVector values)
        {
            var result = new SeriesCollection();
            for(int i = 0; i < values.Dimensions; i++)
            {
                result.Add(
                    new PieSeries
                    {
                        Title = names[i],
                        Values = new ChartValues<double>() { values[i] },
                        DataLabels = true
                    });
            }

            return result;
        }
    }
}
