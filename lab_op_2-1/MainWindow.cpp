#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QGraphicsDropShadowEffect>
#include <QtMath>
#include <stdlib.h>

double firstValue = 0.0;
int operation = 0;
int clearInput = 0;
double memValue = 0.0;
bool memValueExist = false;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    QGraphicsDropShadowEffect *effects = new QGraphicsDropShadowEffect[24];
    for(int i = 0; i < 23; i++)
    {
        (effects+i)->setBlurRadius(10);
        (effects+i)->setXOffset(2);
        (effects+i)->setYOffset(2);
        (effects+i)->setColor(QColor(0, 0, 0, 60));
    }

    ui->ButtonNum0->setGraphicsEffect(effects);
    ui->ButtonNum1->setGraphicsEffect(effects + 1);
    ui->ButtonNum2->setGraphicsEffect(effects + 2);
    ui->ButtonNum3->setGraphicsEffect(effects + 3);
    ui->ButtonNum4->setGraphicsEffect(effects + 4);
    ui->ButtonNum5->setGraphicsEffect(effects + 5);
    ui->ButtonNum6->setGraphicsEffect(effects + 6);
    ui->ButtonNum7->setGraphicsEffect(effects + 7);
    ui->ButtonNum8->setGraphicsEffect(effects + 8);
    ui->ButtonNum9->setGraphicsEffect(effects + 9);
    ui->ButtonPlusMinus->setGraphicsEffect(effects + 10);
    ui->ButtonDot->setGraphicsEffect(effects + 11);
    ui->ButtonPow->setGraphicsEffect(effects + 12);
    ui->ButtonDel->setGraphicsEffect(effects + 13);
    ui->ButtonMemC->setGraphicsEffect(effects + 14);
    ui->ButtonMemR->setGraphicsEffect(effects + 15);
    ui->ButtonMemPlus->setGraphicsEffect(effects + 16);
    ui->ButtonMemMinus->setGraphicsEffect(effects + 17);
    ui->ButtonSqrt->setGraphicsEffect(effects + 18);
    ui->ButtonDivX->setGraphicsEffect(effects + 19);
    ui->ButtonSin->setGraphicsEffect(effects + 20);
    ui->ButtonCos->setGraphicsEffect(effects + 21);
    ui->ButtonTg->setGraphicsEffect(effects + 22);

    connect(ui->ButtonNum0, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum1, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum2, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum3, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum4, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum5, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum6, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum7, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum8, SIGNAL(clicked()), this, SLOT(ButtonNum()));
    connect(ui->ButtonNum9, SIGNAL(clicked()), this, SLOT(ButtonNum()));

    connect(ui->ButtonPlus, SIGNAL(clicked()), this, SLOT(ButtonOperation()));
    connect(ui->ButtonMinus, SIGNAL(clicked()), this, SLOT(ButtonOperation()));
    connect(ui->ButtonMult, SIGNAL(clicked()), this, SLOT(ButtonOperation()));
    connect(ui->ButtonDiv, SIGNAL(clicked()), this, SLOT(ButtonOperation()));
    connect(ui->ButtonPow, SIGNAL(clicked()), this, SLOT(ButtonOperation()));

    connect(ui->ButtonSin, SIGNAL(clicked()), this, SLOT(TrigonometryOperation()));
    connect(ui->ButtonCos, SIGNAL(clicked()), this, SLOT(TrigonometryOperation()));
    connect(ui->ButtonTg, SIGNAL(clicked()), this, SLOT(TrigonometryOperation()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_ButtonClear_clicked()
{
    ui->LabelAnswer->setText("0");
    operation = 0;
    firstValue = 0.0;
    clearInput = 0;
    memValue = 0.0;
    memValueExist = false;
}

void MainWindow::on_ButtonDot_clicked()
{
    QString oldText = ui->LabelAnswer->text();
    if(!oldText.contains('.'))
    {
        if(!oldText.length())
            oldText = "0";
        oldText += '.';
    }
    ui->LabelAnswer->setText(oldText);
}

void MainWindow::on_ButtonPlusMinus_clicked()
{
    QString text = ui->LabelAnswer->text();
    if(!text.length())
        text = "0";
    if(text.toDouble() > 0.0)
        text.prepend('-');
    else if(text.toDouble() < 0.0)
        text = text.right(text.length() - 1);
    ui->LabelAnswer->setText(text);
}

void MainWindow::on_ButtonDel_clicked()
{
    QString text = ui->LabelAnswer->text();
    int length = text.length();
    if(length == 1)
        text = "0";
    else if(length >= 2 && text[length - 2] == '.')
        text = text.left(length - 2);
    else
        text = text.left(length - 1);
    ui->LabelAnswer->setText(text);
}

void MainWindow::on_ButtonEqually_clicked()
{
    QString text = ui->LabelAnswer->text();
    if(operation && text.length())
    {
        switch(operation)
        {
            case 1: // +
                text = QString::number(firstValue + text.toDouble(), 'g', 15);
                break;
            case 2: // -
                text = QString::number(firstValue - text.toDouble(), 'g', 15);
                break;
            case 3: // /
                if(text.toDouble() == 0.0)
                    text = "ERR";
                text = QString::number(firstValue / text.toDouble(), 'g', 15);
                break;
            case 4: // *
                text = QString::number(firstValue * text.toDouble(), 'g', 15);
                break;
            case 5: // pow
                text = QString::number(qPow(firstValue, text.toDouble()), 'g', 15);
                break;
       }
        firstValue = 0.0;
        operation = 0;
        ui->LabelAnswer->setText(text);
        clearInput = 1;
    }
}

void MainWindow::ButtonNum()
{
    QPushButton *btn = (QPushButton *) sender();
    QString oldText = ui->LabelAnswer->text();
    if(clearInput)
    {
        clearInput = 0;
        oldText = "";
    }
    if(oldText.length() < 15)
    {
        if(oldText.contains('.'))
            ui->LabelAnswer->setText(ui->LabelAnswer->text() + btn->text());
        else
            ui->LabelAnswer->setText(QString::number((ui->LabelAnswer->text() + btn->text()).toDouble(), 'g', 15));

    }
}

void MainWindow::ButtonOperation()
{
    QPushButton *btn = (QPushButton *) sender();
    if(btn->text() == "+")
        operation = 1;
    else if(btn->text() == "-")
        operation = 2;
    else if(btn->text() == "÷")
        operation = 3;
    else if(btn->text() == "×")
        operation = 4;
    else if(btn->text() == "pow")
        operation = 5;
    firstValue = ui->LabelAnswer->text().toDouble();
    ui->LabelAnswer->setText("");
}

void MainWindow::on_ButtonSqrt_clicked()
{
    double value = ui->LabelAnswer->text().toDouble();
    value = sqrt(value);
    ui->LabelAnswer->setText(QString::number(value, 'g', 15));
    clearInput = 1;
}

void MainWindow::on_ButtonDivX_clicked()
{
    double value = ui->LabelAnswer->text().toDouble();
    if(value == 0.0)
        ui->LabelAnswer->setText("ERR");
    else
    {
        value = 1 / value;
        ui->LabelAnswer->setText(QString::number(value, 'g', 15));
    }
    clearInput = 1;
}

void MainWindow::TrigonometryOperation()
{
    double value = ui->LabelAnswer->text().toDouble();
    value = qDegreesToRadians(value);
    QPushButton *btn = (QPushButton *) sender();
    QString newText = "";
    if(btn->text() == "sin")
        newText = QString::number(sin(value), 'g', 15);
    else if(btn->text() == "cos")
        newText = QString::number(cos(value), 'g', 15);
    else if(btn->text() == "tg")
    {
        if(!fmod(value, M_PI_2) || !fmod(value, M_PI_2 * 3))
            newText = "ERR";
        else
            newText = QString::number(tan(value), 'g', 15);
    }
    ui->LabelAnswer->setText(newText);
    clearInput = 1;
}

void MainWindow::on_ButtonMemPlus_clicked()
{
    memValueExist = true;
    memValue += ui->LabelAnswer->text().toDouble();
    ui->LabelMem->setText("Mem: " + QString::number(memValue, 'g', 15));
}

void MainWindow::on_ButtonMemMinus_clicked()
{
    memValueExist = true;
    memValue -= ui->LabelAnswer->text().toDouble();
    ui->LabelMem->setText("Mem: " + QString::number(memValue, 'g', 15));
}

void MainWindow::on_ButtonMemR_clicked()
{
    if(memValueExist)
    {
        ui->LabelAnswer->setText(QString::number(memValue, 'g', 15));
        clearInput = 0;
    }
}

void MainWindow::on_ButtonMemC_clicked()
{
    memValue = 0.0;
    memValueExist = false;
    ui->LabelMem->setText("");
}
