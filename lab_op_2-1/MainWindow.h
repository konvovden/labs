#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ButtonClear_clicked();

    void on_ButtonDot_clicked();

    void on_ButtonPlusMinus_clicked();

    void on_ButtonDel_clicked();

    void on_ButtonEqually_clicked();

    void ButtonNum();

    void ButtonOperation();

    void on_ButtonSqrt_clicked();

    void on_ButtonDivX_clicked();

    void TrigonometryOperation();

    void on_ButtonMemPlus_clicked();

    void on_ButtonMemMinus_clicked();

    void on_ButtonMemR_clicked();

    void on_ButtonMemC_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
