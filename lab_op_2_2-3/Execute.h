#ifndef EXECUTE_H
#define EXECUTE_H

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

enum operation_type
{
    TABLE_LOAD_HEADERS = 0,
    TABLE_LOAD_DATA = 1,
    TABLE_METRICS = 2,
    TABLE_CLEAR_CACHE = 3,
    TABLE_GRAPHIC = 4
};

enum error_type
{
    NO_ERROR = 0,
    FILE_NOT_OPENED = 1,
    FILE_EMPTY = 2,
    METRICS_NOT_NUMBER = 3,
    METRICS_NO_ROWS = 4,
    GRAPHICS_NO_ROWS = 5,
    GRAPHICS_NOT_NUMBER = 6
};

struct operation_t
{
    operation_type type;
    string fileName;
    int findCol;
    string findText;
    int findGapCol;
    string findGapMinText;
    string findGapMaxText;
    int metricsCol;
    int graphicX;
    int graphicY;
};

struct result_t
{
    vector<string> headers;
    vector<vector<string>> table;
    double min;
    double max;
    double middle;
    double median;
    error_type error;
    vector<vector<double>> graphicValues;
    int graphicsYMinIndex;
    int graphicsYMaxIndex;
    int graphicsYMiddleIndex;
};

result_t *executeOperation(operation_t *operation);


#endif // EXECUTE_H
