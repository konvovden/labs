#pragma once

#include <iostream>
#include <fstream>
#include <string.h>
#include <sstream>
#include "Strings.h"
#include "Arrays.h"
#include "Execute.h"
//#include "DoubleVectors.h"
#include <algorithm>
#include "VectorOperations.h"
#include "DataCache.h"




error_type loadHeadersFromFile(vector<string> *headers, const string fileName);
error_type loadDataFromFile(vector<vector<string>> *data, const operation_t *operation);
error_type calcMetrics(result_t *result, const operation_t *operation);
error_type caclGraphicValues(result_t *result, const operation_t *operation);
