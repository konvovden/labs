#include "Execute.h"
#include "Files.h"

result_t *executeOperation(operation_t *operation)
{
    result_t *result = new result_t;
    switch(operation->type)
    {
        case TABLE_LOAD_HEADERS:
            result->error = loadHeadersFromFile(&result->headers, operation->fileName);
        break;
        case TABLE_LOAD_DATA:
            result->error = loadDataFromFile(&result->table, operation);
        break;
        case TABLE_METRICS:
            result->error = calcMetrics(result, operation);
        break;
        case TABLE_CLEAR_CACHE:
            result->error = NO_ERROR;
            clearDataCache();
        break;
        case TABLE_GRAPHIC:
            result->error = caclGraphicValues(result, operation);
        break;
    }
    return result;
}
