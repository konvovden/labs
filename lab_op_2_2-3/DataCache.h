#ifndef DATACACHE_H
#define DATACACHE_H

#include <iostream>
#include <vector>

using namespace std;

struct DataCache
{
    vector<vector<string>> table;
    bool tableLoaded;
};

void clearDataCache();
DataCache getDataCache();
void setDataCache(DataCache &data);

#endif // DATACACHE_H
