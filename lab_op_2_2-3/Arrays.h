#ifndef ARRAYS_H
#define ARRAYS_H

void sortDoubleArray(double * array, int size);
int getDoubleArrayMinIndex(double * array, int size);
void swap(double *val1, double *val2);

#endif // ARRAYS_H
