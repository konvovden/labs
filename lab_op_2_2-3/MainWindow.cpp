#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "qfiledialog.h"
#include <QStandardItemModel>
#include <QMessageBox>
#include <QGraphicsOpacityEffect>
#include <QPicture>
#include <QPainter>

static QString fileSource = "";
static QStringList headers;
static bool dataLoaded = false;

static int searchIndex = -1;
static QString searchText = "";

static int searchGapIndex = -1;
static QString searchGapMinText = "";
static QString searchGapMaxText = "";

static QStandardItemModel * model = new QStandardItemModel;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    clear();
}

MainWindow::~MainWindow()
{
    delete ui;
    delete model;
}

void MainWindow::on_ButtonChooseFile_clicked()
{
    QString file = QFileDialog::getOpenFileName(this, "Open File", "", "Таблица CSV (*.csv)");
    if(!file.length())
        return;

    fileSource = file;

    operation_t operation;
    operation.type = TABLE_LOAD_HEADERS;
    operation.fileName = file.toStdString();

    result_t *result = executeOperation(&operation);
    if(result->error == NO_ERROR)
    {
        clear();
        fileSource = file;
        ui->label->setText("Файл: " + fileSource.right(fileSource.lastIndexOf('/') - 2));
        setHeaders(result->headers);
    }
    setError(result->error);
    delete result;
}

void MainWindow::setHeaders(vector<string> vheaders)
{
    headers.clear();
    int size = vheaders.size();
    for(int i = 0; i < size; i++)
        headers.append(QString::fromStdString(vheaders[i]));

    setSearchFields();
}

void MainWindow::setTableHeaders()
{
    model->setHorizontalHeaderLabels(headers);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void MainWindow::setSearchFields()
{
    clearSearchFields();

    ui->ComboSearchField->addItem("");
    ui->ComboSearchGapField->addItem("");
    ui->ComboMetricsField->addItem("");
    ui->ComboGraphicX->addItem("");
    ui->ComboGraphicY->addItem("");

    ui->ComboSearchField->addItems(headers);
    ui->ComboSearchGapField->addItems(headers);
    ui->ComboMetricsField->addItems(headers);
    ui->ComboGraphicX->addItems(headers);
    ui->ComboGraphicY->addItems(headers);
}

void MainWindow::clearSearchFields()
{
    searchIndex = -1;
    searchText = "";

    searchGapIndex = -1;
    searchGapMinText = "";
    searchGapMaxText = "";

    ui->ComboSearchField->clear();
    ui->ComboSearchGapField->clear();
    ui->ComboMetricsField->clear();
    ui->ComboGraphicX->clear();
    ui->ComboGraphicY->clear();

    ui->InputSearch->clear();
    ui->InputSearchGapMax->clear();
    ui->InputSearchGapMin->clear();
}

void MainWindow::setError(error_type error)
{
    switch(error)
    {
        case NO_ERROR:
            clearError();
        break;
        case FILE_NOT_OPENED:
            ui->LabelFileError->setText("Ошибка при открытии файла!");
        break;
        case FILE_EMPTY:
            ui->LabelFileError->setText("Ошибка! Файл пуст!");
        break;
        case METRICS_NOT_NUMBER:
            ui->LabelMetricsError->setText("Столбец содержит не числовые данные!");
        break;
        case METRICS_NO_ROWS:
            ui->LabelMetricsError->setText("Таблица не содержит записей!");
        break;
        case GRAPHICS_NO_ROWS:
            ui->LabelGraphicError->setText("Таблица не содержит записей!");
        break;
        case GRAPHICS_NOT_NUMBER:
            ui->LabelGraphicError->setText("Столбцы содержат не числовые данные!");
        break;
    }
}

void MainWindow::clearError()
{
    ui->LabelFileError->setText("");
    ui->LabelSearchError->setText("");
    ui->LabelMetricsError->setText("");
    ui->LabelSearchGapError->setText("");
    ui->LabelGraphicError->setText("");
}

void MainWindow::on_ButtonLoadData_clicked()
{
    if(!headers.size() || dataLoaded)
        return;
    loadData();
}

void MainWindow::loadData()
{
    operation_t operation;
    operation.type = TABLE_LOAD_DATA;
    operation.fileName = fileSource.toStdString();
    operation.findCol = searchIndex;
    operation.findText = searchText.toStdString();
    operation.findGapCol = searchGapIndex;
    operation.findGapMinText = searchGapMinText.toStdString();
    operation.findGapMaxText = searchGapMaxText.toStdString();

    result_t *result = executeOperation(&operation);
    if(result->error == NO_ERROR)
    {
        clearTableContains();
        int rows = result->table.size();
        if(rows)
        {
            int cols = result->table[0].size();
            for(int i = 0; i < rows; i++)
            {
                for(int j = 0; j < cols; j++)
                {
                    QStandardItem *item = new QStandardItem(QString::fromStdString(result->table[i][j]));
                    model->setItem(i, j, item);
                }
            }
        }
        ui->tableView->setModel(model);
        dataLoaded = true;
    }
    setError(result->error);
    delete result;
}

void MainWindow::clearTableContains()
{
    delete model;
    model = new QStandardItemModel;
    setTableHeaders();
}

void MainWindow::clear()
{
    fileSource = "";
    headers.clear();
    clearError();

    clearTableContains();
    ui->tableView->setModel(model);

    clearSearchFields();

    dataLoaded = false;
    operation_t operation;
    operation.type = TABLE_CLEAR_CACHE;
    executeOperation(&operation);
    clearGraphic();
}

void MainWindow::on_ButtonSearch_clicked()
{
    if(!headers.size())
        return;

    int newSearchIndex = ui->ComboSearchField->currentIndex() - 1;
    QString newSearchText = ui->InputSearch->text();
    if(searchIndex == -1 && newSearchIndex == -1)
        ui->LabelSearchError->setText("Не выбран столбец для поиска!");
    else if(newSearchIndex != -1 && !newSearchText.length())
        ui->LabelSearchError->setText("Не введён текст для поиска!");
    else
    {
        if(newSearchIndex == -1)
        {
            newSearchText = "";
            ui->InputSearch->clear();
        }
        ui->LabelSearchError->setText("");
        searchIndex = newSearchIndex;
        searchText = newSearchText;

        loadData();
    }
}

void MainWindow::on_ButtonSearchGap_clicked()
{
    if(!headers.size())
        return;

    int newSearchIndex = ui->ComboSearchGapField->currentIndex() - 1;
    QString newSearchTextMin = ui->InputSearchGapMin->text();
    QString newSearchTextMax = ui->InputSearchGapMax->text();
    if(searchGapIndex == -1 && newSearchIndex == -1)
        ui->LabelSearchGapError->setText("Не выбран столбец для поиска!");
    else if(newSearchIndex != -1 && !newSearchTextMin.length() && !newSearchTextMax.length())
        ui->LabelSearchGapError->setText("Не введены границы для поиска!");
    else
    {
        if(newSearchIndex == -1)
        {
            newSearchTextMin = "";
            newSearchTextMax = "";
            ui->InputSearchGapMin->clear();
            ui->InputSearchGapMax->clear();
        }
        ui->LabelSearchGapError->setText("");
        searchGapIndex = newSearchIndex;
        searchGapMinText = newSearchTextMin;
        searchGapMaxText = newSearchTextMax;

        loadData();
    }
}

void MainWindow::on_ButtonMetrics_clicked()
{
    if(!headers.size())
        return;

    int metricsIndex = ui->ComboMetricsField->currentIndex() - 1;
    if(metricsIndex == -1)
        ui->LabelMetricsError->setText("Не выбран столбец!");
    else if(!dataLoaded)
        ui->LabelMetricsError->setText("Данные не загружены!");
    else
    {
        operation_t operation;
        operation.type = TABLE_METRICS;
        operation.fileName = fileSource.toStdString();
        operation.findCol = searchIndex;
        operation.findText = searchText.toStdString();
        operation.findGapCol = searchGapIndex;
        operation.findGapMinText = searchGapMinText.toStdString();
        operation.findGapMaxText = searchGapMaxText.toStdString();
        operation.metricsCol = metricsIndex;

        result_t *result = executeOperation(&operation);

        if(result->error == NO_ERROR)
        {
            ui->LabelMetricsError->setText("");
            QMessageBox dialog;

            QString dialogText = "\
Минимальное значение: " + QString::number(result->min) + "\n\
Максимальное значение: " + QString::number(result->max) + "\n\n\
Медиана: " + QString::number(result->median) + "\n\
Среднее арифметическое: " + QString::number(result->middle);
            dialog.setText(dialogText);
            dialog.exec();
        }

        setError(result->error);
        delete result;
    }
}

void MainWindow::on_ButtonGraphic_clicked()
{
    if(!dataLoaded)
        return;

    int colXIndex = ui->ComboGraphicX->currentIndex()-1;
    int colYIndex = ui->ComboGraphicY->currentIndex()-1;
    if(colXIndex == -1 || colYIndex == -1)
    {
        ui->LabelGraphicError->setText("Оси не выбраны!");
        return;
    }

    operation_t operation;
    operation.type = TABLE_GRAPHIC;
    operation.graphicX = colXIndex;
    operation.graphicY = colYIndex;
    result_t *result = executeOperation(&operation);

    if(result->error == NO_ERROR)
    {
        const int sizeX = 920;
        const int sizeY = 265;

        const int padding = 10;
        const int graphicYOffset = 30;
        const int graphicXOffset = 20;

        QPicture picture;
        picture.setBoundingRect(QRect(QPoint(0, 0), QPoint(sizeX, sizeY)));
        QPainter painter;

        painter.begin(&picture);

        QPen dotsPen = painter.pen();
        dotsPen.setWidth(2);
        dotsPen.setColor(QColor(255, 0, 0));

        QPen defPen = painter.pen();
        defPen.setWidth(2);

        QPen horPen = painter.pen();
        horPen.setStyle(Qt::DotLine);

        painter.setPen(defPen);

        const int horYPos = sizeY - padding - graphicYOffset;
        const int verXPos = padding + graphicXOffset;

        painter.drawLine(verXPos, horYPos, sizeX - padding, horYPos); // horizontal line
        painter.drawLine(sizeX - padding, horYPos, sizeX - padding - 5, horYPos - 3); // horizontal line arrow parts
        painter.drawLine(sizeX - padding, horYPos, sizeX - padding - 5, horYPos + 3);

        painter.drawLine(verXPos, horYPos, verXPos, padding); // vertical line
        painter.drawLine(verXPos, padding, verXPos - 3, padding + 5); // vertical line arrows parts
        painter.drawLine(verXPos, padding, verXPos + 3, padding + 5);

        int size = result->graphicValues.size();
        double minX = result->graphicValues[0][0];
        double diffX = result->graphicValues[size - 1][0] - minX;

        double minY = result->graphicValues[result->graphicsYMinIndex][1];
        double diffY = result->graphicValues[result->graphicsYMaxIndex][1] - minY;
        for(int i = 0; i < size; i++)
        {
            double posX = verXPos + 5 + ((result->graphicValues[i][0] - minX) / diffX) * (sizeX - 3 * padding - verXPos);
            double posY = horYPos - padding - ((result->graphicValues[i][1] - minY) / diffY) * (horYPos - 4 * padding);

            painter.setPen(defPen);
            painter.drawLine(posX + 1, horYPos + 2, posX + 1, horYPos - 2); // segments on horizontal line

            painter.rotate(-90);
            painter.drawText(-(sizeY - padding/2), posX + 5, QString::number(result->graphicValues[i][0], 'g', 6)); // text under horizontal
            painter.rotate(90);

            if(i == result->graphicsYMaxIndex || i == result->graphicsYMinIndex || i == result->graphicsYMiddleIndex)
            {
                painter.drawLine(verXPos - 2, posY, verXPos + 2, posY);
                painter.drawText(0, posY - 1, QString::number(result->graphicValues[i][1], 'g', 4));
                painter.setPen(horPen);
                painter.drawLine(verXPos, posY, posX, posY);
            }

            painter.setPen(dotsPen);
            painter.drawEllipse(posX, posY, 2, 2); // dots
        }

        painter.end();

        ui->graphics->setPicture(picture);
    }
    setError(result->error);

    delete result;
}

void MainWindow::clearGraphic()
{
    QPicture picture;
    ui->graphics->setPicture(picture);
}

void MainWindow::on_pushButton_clicked()
{

}
