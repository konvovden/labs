#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Execute.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_ButtonChooseFile_clicked();

    void on_ButtonLoadData_clicked();

    void on_ButtonSearch_clicked();

    void on_ButtonSearchGap_clicked();

    void on_ButtonMetrics_clicked();

    void on_ButtonGraphic_clicked();

    void on_pushButton_clicked();

private:
    void setError(error_type error);
    void clearError();
    void clear();
    void setHeaders(vector<string> headers);
    void clearSearchFields();
    void setSearchFields();
    void setTableHeaders();
    void clearTableContains();
    void loadData();
    void clearGraphic();

private:
    Ui::MainWindow *ui;
};

int CheckError(int status);
#endif // MAINWINDOW_H
