#include <Arrays.h>

void sortDoubleArray(double * array, int size)
{
    for(int i = 0; i < size; i++)
    {
        int minIndex = i + getDoubleArrayMinIndex(array + i, size - i);
        swap(array + i, array + minIndex);
    }
}

int getDoubleArrayMinIndex(double * array, int size)
{
    int index = 0;
    for(int i = 1 ; i < size; i++)
    {
        if(array[i] < array[index])
            index = i;
    }

    return index;
}

void swap(double *val1, double *val2)
{
    double temp = *val2;
    *val2 = *val1;
    *val1 = temp;
}
