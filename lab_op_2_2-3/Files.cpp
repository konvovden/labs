#include "Files.h"

error_type loadHeadersFromFile(vector<string> *headers, const string fileName)
{
    fstream file (fileName);
    if(!file)
        return FILE_NOT_OPENED;

    string row = "";
    getline(file, row);
    if(!row.length())
        return FILE_EMPTY;

    headers->clear();
    vector<string> headerWords = separateString(row, ',');
    int colSize = headerWords.size();
    for(int i = 0; i < colSize; i++)
        headers->push_back(headerWords[i]);



    file.close();
    return NO_ERROR;
}

error_type loadDataFromFile(vector<vector<string>> *data, const operation_t *operation)
{
    fstream file (operation->fileName);
    if(!file)
        return FILE_NOT_OPENED;

    string row = "";
    getline(file, row);
    if(!row.length())
        return FILE_EMPTY;

    unsigned int cols = separateString(row, ',').size();
    DataCache cache;


    while(getline(file, row))
    {
        vector<string> fields = separateString(row, ',');
        while(fields.size() < cols)
            fields.push_back("");
        bool status = true;
        if(operation->findCol != -1 && operation->findText != fields[operation->findCol])
            status = false;
        else if(operation->findGapCol != -1 &&
                ((operation->findGapMinText.length () && fields[operation->findGapCol].compare(operation->findGapMinText) < 0)
                 || (operation->findGapMaxText.length() && fields[operation->findGapCol].compare(operation->findGapMaxText) > 0)))
            status = false;

        if(status)
        {
            data->push_back(fields);
            cache.table.push_back(fields);
            cache.tableLoaded = true;
        }
    }

    setDataCache(cache);

    file.close();
    return NO_ERROR;
}

error_type calcMetrics(result_t *result, const operation_t *operation)
{
    error_type status = NO_ERROR;
    DataCache cache = getDataCache();
    int rows = cache.table.size();
    int col = operation->metricsCol;
    if(!rows)
        status = METRICS_NO_ROWS;
    else
    {
        vector<double> content;
        for(int i = 0 ; i < rows; i++)
        {

            string element = cache.table[i][col];
            bool ok = isDouble(element);
            if(!ok && element.length())
            {
                status = METRICS_NOT_NUMBER;
                break;
            }

            double value;

            if(element.length())
                value = stod(element);
            else
                value = 0.0;

            content.push_back(value);
        }
        if(status == NO_ERROR)
        {
            result->min = findDoubleVectorMinValue(content);
            result->max = findDoubleVectorMaxValue(content);
            result->median = findDoubleVectorMedianValue(content);
            result->middle = calcDoubleVectorMiddleValue(content);
        }
    }
    return status;
}

error_type caclGraphicValues(result_t *result, const operation_t *operation)
{
    DataCache cache = getDataCache();
    int rows = cache.table.size();
    if(!rows)
        return GRAPHICS_NO_ROWS;

    int colX = operation->graphicX;
    int colY = operation->graphicY;

    vector<vector<double>> temp;
    for(int i = 0; i < rows; i++)
    {
        vector<double> row;
        if(!isDouble(cache.table[i][colX]) || !isDouble(cache.table[i][colY]))
            return GRAPHICS_NOT_NUMBER;

        if(cache.table[i][colX].length())
            row.push_back(stod(cache.table[i][colX]));
        else
            row.push_back(0.0);

        if(cache.table[i][colY].length())
            row.push_back(stod(cache.table[i][colY]));
        else
            row.push_back(0.0);
        temp.push_back(row);
    }

    sort(temp.rbegin(), temp.rend(), doubleVectorComp);

    recalcRepeatsInDoubleVector(temp);

    result->graphicsYMinIndex = 0;
    result->graphicsYMaxIndex = 0;

    double middle = calcDoubleVectorsMiddleValue(temp, 1);
    int middleIndex = getDoubleVectorsNearestElement(temp, middle, 1);

    result->graphicsYMinIndex = findDoubleVectorsMinIndex(temp, 1);
    result->graphicsYMaxIndex = findDoubleVectorsMaxIndex(temp, 1);

    result->graphicsYMiddleIndex = middleIndex;
    result->graphicValues = temp;
    return NO_ERROR;
}



