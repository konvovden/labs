#ifndef VECTOROPERATIONS_H
#define VECTOROPERATIONS_H

#include <vector>
#include <string>

using namespace std;

bool doubleVectorComp(vector<double> val1, vector<double> val2);
void recalcRepeatsInDoubleVector(vector<vector<double>> &source);

int findDoubleVectorsMinIndex(vector<vector<double>> &source, int col = 0);
double findDoubleVectorsMinValue(vector<vector<double>> &source, int col = 0);
int findDoubleVectorsMaxIndex(vector<vector<double>> &source, int col = 0);
double findDoubleVectorsMaxValue(vector<vector<double>> &source, int col = 0);

int findDoubleVectorMinIndex(vector<double> &source, int start = 0);
double findDoubleVectorMinValue(vector<double> &source);
int findDoubleVectorMaxIndex(vector<double> &source);
double findDoubleVectorMaxValue(vector<double> &source);

void sortDoubleVector(vector<double> &source);
double findDoubleVectorMedianValue(vector<double> &source);

double calcDoubleVectorsMiddleValue(vector<vector<double>> &source, int col = 0);
int getDoubleVectorsNearestElement(vector<vector<double>> &source, double value, int col = 0);

double calcDoubleVectorSum(vector<double> &source);
double calcDoubleVectorMiddleValue(vector<double> &source);
#endif // VECTOROPERATIONS_H
