#include "DataCache.h"

static DataCache cache;

void clearDataCache()
{
    cache.table.clear();
    cache.tableLoaded = false;
}

DataCache getDataCache()
{
    return cache;
}

void setDataCache(DataCache &data)
{
    cache = data;
}
