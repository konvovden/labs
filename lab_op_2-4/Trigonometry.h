#ifndef TRIGONOMETRY_H
#define TRIGONOMETRY_H

float fromRadToAng(float rad);
float fromAngToRad(float ang);


#endif // TRIGONOMETRY_H
