#include "Files.h"

#include <fstream>
#include "Strings.h"

using namespace std;


int loadDataFromFile(vector<vector<string>> &data, const string fileName)
{
    fstream file (fileName);
    if(!file)
        return 1;

    string row = "";

    unsigned int cols = 0;

    while(getline(file, row))
    {
        vector<string> fields = separateString(row, ',');
        if(!cols)
            cols = fields.size();
        while(fields.size() < cols)
            fields.push_back("");
        data.push_back(fields);
    }

    file.close();
    return 0;
}
