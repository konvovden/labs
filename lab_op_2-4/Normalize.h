#ifndef NORMALIZE_H
#define NORMALIZE_H

float normalizeValue(float value, float minValue, float maxValue, float minRange, float maxRange);

#endif // NORMALIZE_H
