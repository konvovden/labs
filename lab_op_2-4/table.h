#ifndef TABLE_H
#define TABLE_H
#include <QStandardItemModel>
#include <string.h>
#include <iostream>


using namespace std;

vector<string> separate_string(string str, char sign);
int load_data_from_file(QStandardItemModel* model, string filename);
int load_headers_from_file(QStandardItemModel* model, string filename);


#endif // TABLE_H
