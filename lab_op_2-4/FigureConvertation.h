#ifndef FIGURECONVERTATION_H
#define FIGURECONVERTATION_H

#include "Figure2D.h"
#include "Figure3D.h"

Figure2D convertFigure3DToFigure2D(Figure3D &figure);

#endif // FIGURECONVERTATION_H
